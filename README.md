# pomodoro-rs

A few implementations of [Pomodoro Technique]() timers using various Rust GUI libraries.

## Completed

* SDL
* imgui

## In Progress

* *(none currently)*

## Building on Windows

If you already have the SDL2 DLL files around, and your SDL2 mixer supports Ogg, you
can just `cargo build` the source. If you need SDL2 installed first, [vcpkg](https://github.com/Microsoft/vcpkg) is recommended. Its installation is beyond the scope of this README.

```
$ vcpkg install yasm-tools:x86-windows
$ vcpkg install sdl2 sdl2-gfx sdl2-ttf sdl2-mixer[core,dynamic-load,libvorbis] --triplet=x64-windows
```

Now you should be able to `cargo build` provided the vcpkg install directories are in your `PATH`. Instructions for that are beyond the scope of this README.

### Notes

* [Alert sound on jsfxr.](https://sfxr.me/#111118H1j5i7ZGMeUdeAfHg39s7a8FjPEMfubBpnZzVJfHmEnfjpBWJ9DJdH9qNSswgKtrWno3SBP7wvbeQtZYQYXtqE2PMbwVXijhusqcx5vrKEz8gig5iR)