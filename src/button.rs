use sdl2::mouse::MouseState;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Texture;
use sdl2::render::{Canvas, TextureCreator};
use sdl2::ttf::Font;
use sdl2::video::{Window, WindowContext};

pub struct Button<'a> {
    pub label: &'static str,
    x: i32,
    y: i32,
    pub width: u32,
    pub height: u32,
    pub hovered: bool,
    pub clicked: bool,
    font: &'a Font<'a, 'a>,
    texture_creator: &'a TextureCreator<WindowContext>,
    texture: Texture<'a>,
    label_position: Rect,
}

impl Button<'_> {
    pub fn new<'a>(
        label: &'static str,
        x: u32,
        y: u32,
        width: u32,
        height: u32,
        font: &'a Font<'a, 'a>,
        texture_creator: &'a TextureCreator<WindowContext>,
    ) -> Result<Button<'a>, String> {
        let label_surface = font
            .render(label)
            .solid(Color::RGB(255, 255, 255))
            .map_err(|e| e.to_string())?;
        let label_texture = texture_creator
            .create_texture_from_surface(&label_surface)
            .map_err(|e| e.to_string())?;
        let label_x = x as u32 + (width / 2) - (label_surface.width() / 2);
        let label_y = y as u32 + (height / 2) - (label_surface.height() / 2);

        Ok(Button {
            label,
            x: x as i32,
            y: y as i32,
            width,
            height,
            font,
            hovered: false,
            clicked: false,
            texture_creator,
            texture: label_texture,
            label_position: Rect::new(
                label_x as i32,
                label_y as i32,
                label_surface.width(),
                label_surface.height(),
            ),
        })
    }

    pub fn update_label(&mut self, label: &'static str) -> Result<(), String> {
        let label_surface = self
            .font
            .render(label)
            .solid(Color::RGB(255, 255, 255))
            .map_err(|e| e.to_string())?;
        let label_texture = self
            .texture_creator
            .create_texture_from_surface(&label_surface)
            .map_err(|e| e.to_string())?;
        let label_x = self.x as u32 + (self.width / 2) - (label_surface.width() / 2);
        let label_y = self.y as u32 + (self.height / 2) - (label_surface.height() / 2);

        self.texture = label_texture;
        self.label_position = Rect::new(
            label_x as i32,
            label_y as i32,
            label_surface.width(),
            label_surface.height(),
        );

        Ok(())
    }

    pub fn handle_event(&mut self, mouse: &MouseState) {
        let (x, y) = (mouse.x(), mouse.y());
        let mut inside = true;
        if (x < self.x)
            || (x > self.x + (self.width as i32))
            || (y < self.y)
            || (y > self.y + (self.height as i32))
        {
            inside = false;
        }

        if !inside {
            self.hovered = false;
            self.clicked = false;
        } else {
            self.hovered = true;
            if mouse.left() {
                self.clicked = true;
            } else {
                self.clicked = false;
            }
        }
    }

    pub fn render_to(&self, renderer: &mut Canvas<Window>) -> Result<(), String> {
        // Render button.
        let rect = Rect::new(self.x, self.y, self.width, self.height);

        if self.clicked {
            renderer.set_draw_color(Color::RGB(15, 135, 250));
        } else if !self.clicked && self.hovered {
            renderer.set_draw_color(Color::RGB(66, 150, 250));
        } else {
            renderer.set_draw_color(Color::RGB(41, 74, 122));
        }

        renderer.fill_rect(rect)?;

        // Render label.
        renderer.copy(&self.texture, None, self.label_position)?;

        Ok(())
    }
}
