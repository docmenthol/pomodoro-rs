use serde::Deserialize;
use std::path::Path;

#[derive(Deserialize)]
pub struct Config {
    pub task_duration: u32,
    pub break_duration: u32,
    pub long_break_duration: u32,
}

impl Config {
    pub fn from_file(path: &Path) -> Result<Self, String> {
        let config_str = std::fs::read_to_string(path).map_err(|_| "Error loading config file")?;
        let config_file = toml::from_str(&config_str);

        match config_file {
            Err(err) => Err(format!("Error parsing config: {:?}", err)),
            Ok(config) => Ok(config),
        }
    }
}
