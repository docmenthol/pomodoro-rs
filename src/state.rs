use crate::mode::*;

#[derive(Default, Debug)]
pub struct State {
    pub at_beginning: bool,
    pub stage_one: bool,
    pub stage_two: bool,
    pub stage_three: bool,
    pub stage_four: bool,
    pub running: bool,
    pub confirming: bool,
    pub mode: Mode,
    pub time_left: u32,
    pub mode_config: ModeConfig,
}

impl State {
    pub fn new() -> Self {
        let mut new_state = State::default();
        new_state.at_beginning = true;
        new_state.time_left = new_state.time_for_mode();
        new_state.update_time();
        new_state
    }

    pub fn new_with_config(
        task_duration: u32,
        break_duration: u32,
        long_break_duration: u32,
    ) -> Self {
        let mut new_state = State::new();
        new_state.mode_config = ModeConfig::new(task_duration, break_duration, long_break_duration);
        new_state.update_time();
        new_state
    }

    pub fn start(&mut self) {
        self.running = true;
        if self.at_beginning {
            self.at_beginning = false;
        }
    }

    pub fn stop(&mut self) {
        self.running = false;
    }

    pub fn finish_stage_one(&mut self) {
        self.stage_one = true;
        self.mode = Mode::EnteringBreak;
    }

    pub fn finish_stage_two(&mut self) {
        self.stage_two = true;
        self.mode = Mode::EnteringBreak;
    }

    pub fn finish_stage_three(&mut self) {
        self.stage_three = true;
        self.mode = Mode::EnteringBreak;
    }

    pub fn finish_stage_four(&mut self) {
        self.stage_four = true;
        self.mode = Mode::EnteringLongBreak;
    }

    pub fn finish_stage(&mut self) {
        if !self.stage_one {
            self.finish_stage_one();
        } else if self.stage_one && !self.stage_two {
            self.finish_stage_two();
        } else if self.stage_one && self.stage_two && !self.stage_three {
            self.finish_stage_three();
        } else if self.stage_one && self.stage_two && self.stage_three && !self.stage_four {
            self.finish_stage_four();
        }

        self.update_time();
    }

    pub fn finish_break(&mut self) {
        self.mode = Mode::EnteringTask;
        self.update_time();
    }

    pub fn finish_long_break(&mut self) {
        self.reset_steps();

        self.mode = Mode::EnteringTask;
        self.update_time();
    }

    pub fn confirm_next(&mut self) {
        let new_mode = match self.mode {
            Mode::EnteringTask => Mode::Task,
            Mode::EnteringBreak => Mode::Break,
            Mode::EnteringLongBreak => Mode::LongBreak,
            _ => self.mode,
        };

        self.mode = new_mode;
        // self.finish_stage();
        self.update_time();
    }

    pub fn tick(&mut self, dt: u32) {
        if dt >= self.time_left {
            self.time_left = 0;
        } else {
            self.time_left = self.time_left - dt;
        }
    }

    pub fn time_for_mode(&self) -> u32 {
        match self.mode {
            Mode::Task => self.mode_config.task_duration,
            Mode::Break => self.mode_config.break_duration,
            Mode::LongBreak => self.mode_config.long_break_duration,
            _ => 0,
        }
    }

    pub fn update_time(&mut self) {
        self.time_left = self.time_for_mode();
    }

    pub fn reset_steps(&mut self) {
        self.stage_one = false;
        self.stage_two = false;
        self.stage_three = false;
        self.stage_four = false;
    }

    pub fn reset(&mut self) {
        self.stop();
        self.reset_steps();

        self.at_beginning = true;

        self.mode = Mode::Task;
        self.update_time();
    }
}
