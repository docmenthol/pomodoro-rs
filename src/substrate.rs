use sdl2::{mixer::InitFlag, ttf::Sdl2TtfContext, video::Window, Sdl};

pub fn init(width: u32, height: u32) -> Result<(Sdl, Window, Sdl2TtfContext), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    let ttf = match sdl2::ttf::init() {
        Ok(ttf) => ttf,
        Err(err) => panic!("Could not initialize sdl2_ttf. Error: {}", err),
    };

    sdl2::mixer::open_audio(44100, sdl2::mixer::DEFAULT_FORMAT, 1, 256)?;
    sdl2::mixer::init(InitFlag::OGG)?;

    let window = video
        .window("pomodoro-rs", width, height)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window, ttf))
}
