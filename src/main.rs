extern crate sdl2;

mod button;
mod config;
mod mode;
mod state;
mod substrate;

use crate::{button::Button, config::Config, mode::Mode, state::State};
use sdl2::{
    event::Event,
    gfx::primitives::DrawRenderer,
    mixer::{Channel, Chunk},
    pixels::Color,
    rect::Point,
    rect::Rect,
};
use std::{
    path::Path,
    time::{Duration, Instant},
};

const WIDTH: u32 = 384;
const HEIGHT: u32 = 562;
const PROGRESS_HEIGHT: u32 = 100;
const CONTROL_HEIGHT: u32 = 50;
const TIMER_HEIGHT: u32 = HEIGHT - PROGRESS_HEIGHT - CONTROL_HEIGHT;
const CHECKBOX_COLUMN: u32 = WIDTH / 5;

const BACKGROUND_COLOR: Color = Color::RGB(29, 29, 29);
const TASK_COLOR: Color = Color::RGB(41, 74, 122);
const BREAK_COLOR: Color = Color::RGB(41, 122, 74);
const LONG_BREAK_COLOR: Color = Color::RGB(74, 41, 122);
const WHITE: Color = Color::RGBA(255, 255, 255, 255);

const START_LABEL: &'static str = "start";
const PAUSE_LABEL: &'static str = "pause";
const RESUME_LABEL: &'static str = "resume";

fn main() -> Result<(), String> {
    let (sdl_context, window, ttf_context) = crate::substrate::init(WIDTH, HEIGHT)?;

    let cfg = Config::from_file(Path::new("./resources/config.toml"))?;

    let mut renderer = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;

    let texture_creator = renderer.texture_creator();

    let timer_center = Point::new(WIDTH as i32 / 2, TIMER_HEIGHT as i32 / 2);

    let channel = Channel::all();
    let alert_sound = Chunk::from_file(Path::new("./resources/alert.wav"))?;

    let mut now = Instant::now();
    let mut dt: Duration;

    let font = match ttf_context.load_font(&Path::new("resources/ProggyClean.ttf"), 16) {
        Ok(font) => font,
        Err(err) => panic!("Could not load font. Error: {}", err),
    };

    let mut buttons: [Button; 3] = [
        Button::new(
            START_LABEL,
            5,
            TIMER_HEIGHT + 5,
            WIDTH / 2 - 5,
            CONTROL_HEIGHT - 10,
            &font,
            &texture_creator,
        )?,
        Button::new(
            "reset",
            WIDTH / 2 + 5,
            TIMER_HEIGHT + 5,
            WIDTH / 2 - 10,
            CONTROL_HEIGHT - 10,
            &font,
            &texture_creator,
        )?,
        Button::new(
            "continue",
            timer_center.x as u32 - (WIDTH / 2 - 10) / 2,
            timer_center.y as u32 - (CONTROL_HEIGHT - 10) / 2,
            WIDTH / 2 - 10,
            CONTROL_HEIGHT - 10,
            &font,
            &texture_creator,
        )?,
    ];

    let mut state = State::new_with_config(
        cfg.task_duration * 60000,
        cfg.break_duration * 60000,
        cfg.long_break_duration * 60000,
    );
    let mut mouse_lock = false;
    let mut alert_timer = 0;
    let mut app_running = true;
    let mut event_pump = sdl_context.event_pump()?;

    while app_running {
        dt = now.elapsed();
        now = Instant::now();

        let mouse = event_pump.mouse_state();

        if !mouse.left() {
            mouse_lock = false;
        }

        buttons[0].handle_event(&mouse);
        buttons[1].handle_event(&mouse);

        if buttons[0].clicked && !mouse_lock {
            mouse_lock = true;

            if !state.running {
                buttons[0].update_label(PAUSE_LABEL)?;
                state.start();
            } else {
                buttons[0].update_label(RESUME_LABEL)?;
                state.stop();
            }
        }

        if buttons[1].clicked && !mouse_lock {
            mouse_lock = true;
            buttons[0].update_label(START_LABEL)?;
            state.reset();
        }

        let between_modes = state.mode == Mode::EnteringTask
            || state.mode == Mode::EnteringBreak
            || state.mode == Mode::EnteringLongBreak;

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => app_running = false,
                _ => {}
            }
        }

        renderer.set_draw_color(BACKGROUND_COLOR);

        renderer.clear();

        // Render timer circle.
        let total_time = state.time_for_mode();
        let percent_remaining: f32 = if between_modes {
            0.0
        } else {
            state.time_left as f32 / total_time as f32
        };
        let angle = 360.0 - (360.0 * percent_remaining) + 270.0; // start at the top
        let radius: i16 = WIDTH as i16 / 2 - 20;

        let frame_color = if state.mode == Mode::Task
            || state.mode == Mode::EnteringBreak
            || state.mode == Mode::EnteringLongBreak
        {
            TASK_COLOR
        } else if state.mode == Mode::EnteringTask {
            if state.stage_one {
                BREAK_COLOR
            } else {
                LONG_BREAK_COLOR
            }
        } else if state.mode == Mode::Break {
            BREAK_COLOR
        } else if state.mode == Mode::LongBreak {
            LONG_BREAK_COLOR
        } else {
            TASK_COLOR
        };

        // Render timer progress.
        if percent_remaining > 0.0 && !state.at_beginning {
            renderer.arc(
                timer_center.x as i16,
                timer_center.y as i16,
                radius,
                270,
                angle as i16,
                frame_color,
            )?;

            renderer.arc(
                timer_center.x as i16,
                timer_center.y as i16,
                radius - 2,
                270,
                angle as i16,
                frame_color,
            )?;

            renderer.arc(
                timer_center.x as i16,
                timer_center.y as i16,
                radius - 4,
                270,
                angle as i16,
                frame_color,
            )?;

            renderer.arc(
                timer_center.x as i16,
                timer_center.y as i16,
                radius - 6,
                270,
                angle as i16,
                frame_color,
            )?;
        } else {
            renderer.circle(
                timer_center.x as i16,
                timer_center.y as i16,
                radius,
                frame_color,
            )?;
            renderer.circle(
                timer_center.x as i16,
                timer_center.y as i16,
                radius - 2,
                frame_color,
            )?;
            renderer.circle(
                timer_center.x as i16,
                timer_center.y as i16,
                radius - 4,
                frame_color,
            )?;
            renderer.circle(
                timer_center.x as i16,
                timer_center.y as i16,
                radius - 6,
                frame_color,
            )?;
        }

        // Render timer text.
        let minutes = state.time_left / 60000;
        let seconds = state.time_left / 1000 - (minutes * 60);
        let milliseconds = state.time_left - ((minutes * 60000) + (seconds * 1000));
        let timer_text = format!("{:02}:{:02}.{:03}", minutes, seconds, milliseconds);
        let timer_surface = font
            .render(&timer_text)
            .solid(WHITE)
            .map_err(|e| e.to_string())?;
        let timer_texture = texture_creator
            .create_texture_from_surface(&timer_surface)
            .map_err(|e| e.to_string())?;

        renderer.copy(
            &timer_texture,
            None,
            Rect::new(
                timer_center.x - timer_surface.width() as i32 / 2,
                timer_center.y - timer_surface.height() as i32 / 2,
                timer_surface.width(),
                timer_surface.height(),
            ),
        )?;

        // Render control buttons.
        buttons[0].render_to(&mut renderer)?;
        buttons[1].render_to(&mut renderer)?;

        // Render popup continuation dialog.
        if between_modes {
            buttons[2].handle_event(&mouse);

            renderer.set_draw_color(TASK_COLOR);
            renderer.draw_rect(Rect::new(
                ((WIDTH / 2) - (buttons[2].width / 2)) as i32 - 5,
                ((TIMER_HEIGHT / 2) - (buttons[2].height / 2)) as i32 - 5,
                WIDTH / 2,
                CONTROL_HEIGHT,
            ))?;
            renderer.set_draw_color(BACKGROUND_COLOR);
            renderer.fill_rect(Rect::new(timer_center.x - 49, timer_center.y - 19, 98, 38))?;
            buttons[2].render_to(&mut renderer)?;

            if buttons[2].clicked && !mouse_lock {
                state.confirm_next();
            }
        }

        // Render progress.
        renderer.set_draw_color(TASK_COLOR);

        renderer.draw_rect(Rect::new(
            CHECKBOX_COLUMN as i32 - 12,
            (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 12,
            24,
            24,
        ))?;

        renderer.draw_rect(Rect::new(
            CHECKBOX_COLUMN as i32 * 2 - 12,
            (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 12,
            24,
            24,
        ))?;

        renderer.draw_rect(Rect::new(
            CHECKBOX_COLUMN as i32 * 3 - 12,
            (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 12,
            24,
            24,
        ))?;

        renderer.draw_rect(Rect::new(
            CHECKBOX_COLUMN as i32 * 4 - 12,
            (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 12,
            24,
            24,
        ))?;

        if state.stage_one {
            renderer.fill_rect(Rect::new(
                CHECKBOX_COLUMN as i32 - 9,
                (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 9,
                18,
                18,
            ))?;
        }

        if state.stage_two {
            renderer.fill_rect(Rect::new(
                CHECKBOX_COLUMN as i32 * 2 - 9,
                (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 9,
                18,
                18,
            ))?;
        }

        if state.stage_three {
            renderer.fill_rect(Rect::new(
                CHECKBOX_COLUMN as i32 * 3 - 9,
                (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 9,
                18,
                18,
            ))?;
        }

        if state.stage_four {
            renderer.fill_rect(Rect::new(
                CHECKBOX_COLUMN as i32 * 4 - 9,
                (TIMER_HEIGHT + CONTROL_HEIGHT + PROGRESS_HEIGHT / 2) as i32 - 9,
                18,
                18,
            ))?;
        }

        renderer.present();

        {
            let dtm = dt.subsec_millis();
            if state.running {
                state.tick(dtm);
            }

            if between_modes {
                if alert_timer < 1000 && alert_timer + dtm >= 1000 {
                    channel.play(&alert_sound, 1)?;
                    alert_timer += dtm;
                } else if alert_timer + dtm > 2000 {
                    alert_timer = (alert_timer + dtm) - 2000;
                } else {
                    alert_timer += dtm;
                }
            }

            if state.time_left == 0 {
                match state.mode {
                    Mode::Task => state.finish_stage(),
                    Mode::Break => state.finish_break(),
                    Mode::LongBreak => state.finish_long_break(),
                    _ => {}
                };
            }
        }
    }

    Ok(())
}
