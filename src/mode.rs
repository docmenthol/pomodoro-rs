#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Mode {
    EnteringTask,
    Task,
    EnteringBreak,
    Break,
    EnteringLongBreak,
    LongBreak,
}

impl Default for Mode {
    fn default() -> Self {
        Mode::Task
    }
}

#[derive(Debug)]
pub struct ModeConfig {
    pub task_duration: u32,
    pub break_duration: u32,
    pub long_break_duration: u32,
}

impl Default for ModeConfig {
    fn default() -> Self {
        ModeConfig {
            task_duration: 1_500_000,
            break_duration: 300_000,
            long_break_duration: 1_500_000,
        }
    }
}

impl ModeConfig {
    pub fn new(task_duration: u32, break_duration: u32, long_break_duration: u32) -> Self {
        ModeConfig {
            task_duration,
            break_duration,
            long_break_duration,
        }
    }
}
